#! /bin/sh

# Run this script to regenerate the HTML rendered man pages.
# It assumes that an up-to-date source tree is located
# at a relative directory ../../src.  It performs only dummy
# autoconf-type @VALUE@ substitutions.

INSTTREE=${1-/dev/null}
date=`date +"%Y-%m-%d"`

echo "You should have already % rm *.html; cvs rm; cvs commit"

echo "<html><head><title>systemtap man page index</title></head><body><ul>" > index.html

# fche: last used hand-modified MAN2HTML for proper cross-man page links
# https://bugzilla.redhat.com/show_bug.cgi?id=526112

find $INSTTREE/share/man -type f | sort | while read manpage
do
  htmlpage=`basename $manpage | sed -e 's,$,.html,'`
  # echo -n $manpage
  cat $manpage | 
  sed -e "s,@DATE@,$date," -e "s,@VERSION@,?," | 
  ${MAN2HTML-man2html} -r |
  grep -iv "^Content-Type:" | # we ain't no cgi script
  sed -e "s,../man[1-9][a-z]*/,,g" | # keep everything in this directory
  sed -e "s,../index.html,index.html,g" | # likewise
  sed -e "s,$INSTTREE/,,g" | # get $prefix out of there
  sed -e 's,HREF="function,HREF="./function,g' |
  sed -e 's,HREF="probe,HREF="./probe,g' |
  sed -e 's,HREF="tapset,HREF="./tapset,g' |
  sed -e 's,HREF="error,HREF="./error,g' |
  sed -e 's,HREF="warning,HREF="./warning,g' |
  sed -e 's,\\-,-,g' |
  sed -e 's,../man/man2html,http://www.kapiti.co.nz/michael/vhman2html.html,g' |
  cat > $htmlpage
  # echo " -> $htmlpage"
  echo "<li><a href=\"./$htmlpage\">`basename $manpage`</a></li>" >> index.html
done

echo "</ul></body></html>" >> index.html
echo "Now % cvs add *.html; cvs commit"
